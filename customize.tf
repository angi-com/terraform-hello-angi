locals {
  eks_cluster_name  = "test-eks"
  service_name      = "hello-${local.organization}"
  service_namespace = "default"

  tags = {
    Environment = local.stage
    Terraform   = "true"
  }
}
