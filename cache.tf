resource "aws_elasticache_subnet_group" "cache" {
  name       = "${local.service_name}-cache"
  subnet_ids = module.vpc.private_subnets
}

resource "aws_elasticache_cluster" "cache" {
  cluster_id           = "${local.service_name}-cache"
  engine               = "redis"
  node_type            = "cache.t3.medium"
  num_cache_nodes      = 1
  parameter_group_name = "default.redis3.2"

  apply_immediately    = true
  engine_version       = "3.2.10"
  port                 = 6379

  security_group_ids = [aws_security_group.redis.id]
  subnet_group_name  = aws_elasticache_subnet_group.cache.id

  tags = local.tags
}
