# terraform-hello-angi

This repository contains Terraform code to provision the infrastructure for the hello-angi application.

## Setup
1. Install `tfenv`:
  ```brew install tfenv```
1. Install terraform:
  ```tfenv install 1.1.6```

## Usage
```bash
$ terraform init
$ terraform workspace select dev-us-west-2
Switched to workspace "dev-us-west-2".
$ terraform workspace list
  default
* dev-us-west-2
$ terraform plan -out /tmp/tfplan
$ terraform apply /tmp/tfplan
```
