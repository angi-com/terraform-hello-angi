terraform {
  backend "remote" {
    hostname     = "app.terraform.io"
    organization = "angi-com"

    workspaces {
      prefix = "terraform-hello-angi-"
    }
  }
}

locals {
  organization = "angi"
  profile      = "${local.organization}-${local.stage}"
  region       = join("-", slice(split("-", terraform.workspace), 1, length(split("-", terraform.workspace))))
  stage        = substr(terraform.workspace, 0, 3) # dev, stg, prd
}

provider "aws" {
  profile = local.profile
  region  = local.region
}

provider "kubernetes" {
  config_path = "~/.kube/config"
}

data "aws_caller_identity" "current" {}
