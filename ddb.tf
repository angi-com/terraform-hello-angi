module "ddb_table" {
  source  = "terraform-aws-modules/dynamodb-table/aws"
  version = "1.2.2"

  name     = local.service_name
  hash_key = "ttl"

  attributes = [
    {
      name = "ttl"
      type = "N"
    }
  ]

  ttl_enabled        = true
  ttl_attribute_name = "ttl"

  tags = local.tags
}
