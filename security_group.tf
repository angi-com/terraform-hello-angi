resource "aws_security_group" "redis" {
  name   = "${local.service_name}-cache"
  vpc_id = module.vpc.vpc_id

  ingress {
    protocol  = "tcp"
    from_port = 6379
    to_port   = 6379

    cidr_blocks = [module.vpc.vpc_cidr_block]
  }

  tags = local.tags
}
