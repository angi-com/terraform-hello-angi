resource "kubernetes_service_account" "service" {
  automount_service_account_token = true

  metadata {
    name      = local.service_name
    namespace = local.service_namespace

    annotations = {
      "eks.amazonaws.com/role-arn" = module.service_irsa.iam_role_arn
    }
  }
}
